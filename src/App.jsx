import React, { PureComponent } from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";

import Loading from "./components/containers/loading";
import Landing from "./components/containers/landing";
import Login from "./components/containers/login";
import firebase from "./helpers/firebase";
import actions from "./actions";

class App extends PureComponent {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      this.props.user_set(user);
      this.props.appState_set_updating(false);
    });
  }

  render() {
    if (this.props.appState.loading) {
      return <Route component={Loading} />;
    } else if (!this.props.user) {
      return <Route component={Login} />;
    }

    return <Route component={Landing} />;
  }
}

function mapStateToProps(state) {
  return {
    appState: state.appState,
    user: state.user
  };
}

const mapDispatchToProps = {
  ...actions.appState,
  ...actions.user
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
