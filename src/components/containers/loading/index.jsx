import React, { memo } from "react";
import { FormattedMessage } from "react-intl";
import { Spinner } from "@blueprintjs/core";

export default memo(() => (
  <>
    <Spinner />
    <FormattedMessage id="loading" />
  </>
));
