import React, { PureComponent } from "react";
import PropTypes from 'prop-types';
import { Button as ButtonCore } from "@blueprintjs/core";

class Button extends PureComponent {
  render() {
    return <ButtonCore>{this.props.text}</ButtonCore>;
  }
}

Button.defaultProps = {
  text: "Button"
};

Button.propTypes = {
  text: PropTypes.string
};

export default Button;