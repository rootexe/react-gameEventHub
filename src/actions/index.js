import appState from "./appState";
import user from "./user";

export default {
  appState,
  user
};
