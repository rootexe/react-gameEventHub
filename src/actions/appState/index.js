import types from "./types";

export const appState_set_updating = updating => async dispatch => {
  dispatch({ type: types.APPSTATE_LOADING, payload: updating });
};

export default {
  appState_set_updating
};
